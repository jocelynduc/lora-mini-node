# lora-mini-node

Sample Arduino code for the Mini-Lora node (https://github.com/hallard/Mini-LoRa)

## TODO

* Better implementation of TPL5110 handling
* Better implementation of interrupt

## TTN decoders samples

### Plain text decoder (testing)

	function Decoder(b, port) {
	  // Decode plain text; for testing only 
	  return {
	      decoded: String.fromCharCode.apply(null, b)
	  };
	}

### Custom decoder for the OTAA example

	function Decoder(b, port) {
	  
	  var counter = (b[0] << 8) | b[1];
	  var vcc = (b[2] | b[3] << 8) / 100;
	  var message = String.fromCharCode.apply(null, b.slice(4, 11));

	  return {
	    counter: counter,
	    vcc: vcc,
	    message: message
	  };
	}

## RAK2245

My gateway is a RAK2245. Here are some useful commands and stuffs

* sudo gateway-version
* sudo gateway-config

* /usr/local/rak
* /opt/ttn-network


To display LoRa logs on the gateway to the foreground, you need to execute the following commands in order

* sudo systemctl stop ttn-gateway
* cd /opt/ttn-gateway/packet_forwarder/lora_pkt_fwd/
* sudo ./lora_pkt_fwd

After completing the above steps, the lora log will start to display in the foreground.
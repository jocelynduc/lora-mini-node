/**
 * Copyright 2020 Jocelyn Duc
 * https://bitbucket.org/jocelynduc/
 * 
 * LoRa sender which transmits messages via the RFM95 module. 
 * 
 * LoRa features:
 *  - TTN compatible
 *  - ABP mode
 *  - Acknowledgement enabled 
 *  
 * Low-power features:
 *  - TPL5110 timer allows the node to enter in sleep mode and to be awakened after an interval defined by a resistor
 *  - LowPower library with a time-based sleep mode
 *  - LowPower library with a IRQ-based sleep mode
 *  
 * Based on the Mini-LoRa node designed by Charles Hallard: https://github.com/hallard/Mini-LoRa
 * Based on an example of Adreas Spiess: https://github.com/SensorsIot/LoRa-Point-to-Point
 * Based on an example of crox-net: https://github.com/crox-net/mini-lora-examples
 * 
 * Libraries:
 *  - https://github.com/rocketscream/Low-Power
 *  
 * Utils:
 * - https://platformio.org/lib/show/725/PinChangeInterrupt
 * - https://www.thethingsnetwork.org/docs/devices/bytes.html
 * - https://www.mathworks.com/help/thingspeak/things_network_ag_data.html
 * 
 * Pinout:
 * 
 *  Arduino               RFM9x Module
 *  A0            <---->  RST
 *  D12 (MISO)    <---->  MISO
 *  D11 (MOSI)    <---->  MOSI
 *  D13 (SCK)     <---->  CLK
 *  D10 (SS)      <---->  SEL (Chip Select)
 *  D2            <---->  DIO0
 *  D7            <---->  DIO1
 *  D8            <---->  DIO2
 *  
 *  Arduino               Shield PCB
 *  D3  (INT1)    <---->  Push Button (Has Pull Down)
 *  D9            <---->  Led Red
 *  D6            <---->  Led Green
 *  D5            <---->  Led Blue
 *  A1            <---->  DONE of TPL5110 (to indicate we done)
 *  A2  (PCINT10) <---->  DRVN of TPL5110 (pulse out to wake CPU)
 *  A4  (SDA)     <---->  I2C SCL Connector
 *  A5  (SCL)     <---->  I2C SDA Connector
 *  
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.  
 */

#include <LowPower.h>
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include <RGBLed.h>
#include <credentials.h>

#define PIN_BUTTON 3
#define PIN_DONE A1
#define PIN_DRVN A2
#define PIN_LED_R 9
#define PIN_LED_G 6
#define PIN_LED_B 5
#define PIN_RFM_DIO0 2
#define PIN_RFM_DIO1 7
#define PIN_RFM_DIO2 8
#define PIN_RFM_RST A0
#define PIN_RFM_NSS 10

// LOW POWER CONFIGURATION
// One of these 3 timers can be used:
// - LP_TPL     Sleeps until the TPL DRVN (PCINT10) goes up (resistor-defined timer) or the WAKE button to be pressed
// - LP_TIMER   Sleeps for a period of time defined by LP_TIMER_SLEEP (see LowPower library for possible values)
// - LP_IRQ     Sleeps until the button (D3, INT1) is pressed
//#define LP_TPL
//#define LP_TIMER
//#define LP_TIMER_SLEEP SLEEP_8S
//#define LP_IRQ

// LORA CONFIGURATION
static const PROGMEM u1_t NWKSKEY[16] = CRED_NWKSKEY;
static const u1_t PROGMEM APPSKEY[16] = CRED_APPSKEY;
static const u4_t DEVADDR = CRED_DEVADDR;
void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }

static osjob_t sendjob;

// Schedule TX every this many seconds (might become longer due to duty
// cycle limitations).
const unsigned TX_INTERVAL = 60;

uint16_t counter = 0;
static uint8_t payload[12]; // Number of bytes sent plus 2

// Pin mapping
const lmic_pinmap lmic_pins = {
    .nss = PIN_RFM_NSS,
    .rxtx = LMIC_UNUSED_PIN,
    .rst = PIN_RFM_RST,
    .dio = {PIN_RFM_DIO0, PIN_RFM_DIO1, PIN_RFM_DIO2},
};

RGBLed led(PIN_LED_R, PIN_LED_G, PIN_LED_B, COMMON_ANODE);

void onEvent (ev_t ev) {
  Serial.print(os_getTime());
  Serial.print(": ");
  switch(ev) {
    case EV_SCAN_TIMEOUT:
      Serial.println(F("EV_SCAN_TIMEOUT"));
      break;
    case EV_BEACON_FOUND:
      Serial.println(F("EV_BEACON_FOUND"));
      break;
    case EV_BEACON_MISSED:
      Serial.println(F("EV_BEACON_MISSED"));
      break;
    case EV_BEACON_TRACKED:
      Serial.println(F("EV_BEACON_TRACKED"));
      break;
    case EV_JOINING:
      Serial.println(F("EV_JOINING"));
      break;
    case EV_JOINED:
      Serial.println(F("EV_JOINED"));
      break;
    case EV_RFU1:
      Serial.println(F("EV_RFU1"));
      break;
    case EV_JOIN_FAILED:
      Serial.println(F("EV_JOIN_FAILED"));
      break;
    case EV_REJOIN_FAILED:
      Serial.println(F("EV_REJOIN_FAILED"));
      break;
    case EV_TXCOMPLETE:
      Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
      if (LMIC.txrxFlags & TXRX_ACK)
        Serial.println(F("Received ack"));
      if (LMIC.dataLen) {
        Serial.println(F("Received "));
        Serial.println(LMIC.dataLen);
        Serial.println(F(" bytes of payload"));
      }
      // Schedule next transmission
      os_setTimedCallback(&sendjob, os_getTime()+sec2osticks(TX_INTERVAL), do_send);
      break;
    case EV_LOST_TSYNC:
      Serial.println(F("EV_LOST_TSYNC"));
      break;
    case EV_RESET:
      Serial.println(F("EV_RESET"));
      break;
    case EV_RXCOMPLETE:
      // data received in ping slot
      Serial.println(F("EV_RXCOMPLETE"));
      break;
    case EV_LINK_DEAD:
      Serial.println(F("EV_LINK_DEAD"));
      break;
    case EV_LINK_ALIVE:
      Serial.println(F("EV_LINK_ALIVE"));
      break;
    default:
      Serial.println(F("Unknown event"));
      break;
  }
}

void do_send(osjob_t* j) {

  int vcc = readVcc()/10;

  // Check if there is not a current TX/RX job running
  if (LMIC.opmode & OP_TXRXPEND) {
      Serial.println(F("OP_TXRXPEND, not sending"));
  } else {

    // Field 1: Counter
    payload[0] = byte(counter);
    payload[1] = counter >>8;

    // Field 2: Voltage
    payload[2] = byte(vcc);
    payload[3] = vcc >> 8;

    // Field 3: Message
    uint8_t message[] = "Jocelyn";
    memcpy(payload + 4, message, 7 * sizeof(uint8_t));

    // Prepare upstream data transmission at the next possible time.
    LMIC_setTxData2(1, payload, sizeof(payload) - 1, 1); 
    Serial.println(F("Packet queued"));

    counter++;
  }
  // Next TX is scheduled after TX_COMPLETE event.
}


void setup() {
  
  Serial.begin(9600);
  while (!Serial);
  Serial.println("Starting");
  initPins();

  // LMIC init
  os_init();
  
  // Reset the MAC state. Session and pending data transfers will be discarded.
  LMIC_reset();
  LMIC_setClockError(MAX_CLOCK_ERROR * 1 / 100);

  // Set static session parameters. Instead of dynamically establishing a session
  // by joining the network, precomputed session parameters are be provided.
  #ifdef PROGMEM
    // On AVR, these values are stored in flash and only copied to RAM
    // once. Copy them to a temporary buffer here, LMIC_setSession will
    // copy them into a buffer of its own again.
    uint8_t appskey[sizeof(APPSKEY)];
    uint8_t nwkskey[sizeof(NWKSKEY)];
    memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
    memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
    LMIC_setSession (0x1, DEVADDR, nwkskey, appskey);
  #else
    // If not running an AVR with PROGMEM, just use the arrays directly
    LMIC_setSession (0x1, DEVADDR, NWKSKEY, APPSKEY);
  #endif
  
  #if defined(CFG_eu868)
    // Set up the channels used by the Things Network, which corresponds
    // to the defaults of most gateways. Without this, only three base
    // channels from the LoRaWAN specification are used, which certainly
    // works, so it is good for debugging, but can overload those
    // frequencies, so be sure to configure the full frequency range of
    // your network here (unless your network autoconfigures them).
    // Setting up channels should happen after LMIC_setSession, as that
    // configures the minimal channel set.
    // NA-US channels 0-71 are configured automatically
    LMIC_setupChannel(0, 868100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(1, 868300000, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI);      // g-band
    LMIC_setupChannel(2, 868500000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(3, 867100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(4, 867300000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(5, 867500000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(6, 867700000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(7, 867900000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(8, 868800000, DR_RANGE_MAP(DR_FSK,  DR_FSK),  BAND_MILLI);      // g2-band
    // TTN defines an additional channel at 869.525Mhz using SF9 for class B
    // devices' ping slots. LMIC does not have an easy way to define set this
    // frequency and support for class B is spotty and untested, so this
    // frequency is not configured here.
  #elif defined(CFG_us915)
    // NA-US channels 0-71 are configured automatically
    // but only one group of 8 should (a subband) should be active
    // TTN recommends the second sub band, 1 in a zero based count.
    // https://github.com/TheThingsNetwork/gateway-conf/blob/master/US-global_conf.json
    LMIC_selectSubBand(1);
  #endif
  
  // Disable link check validation
  LMIC_setLinkCheckMode(0);
  
  // TTN uses SF9 for its RX2 window.
  LMIC.dn2Dr = DR_SF9;
  
  // Set data rate and transmit power for uplink (note: txpow seems to be ignored by the library)
  LMIC_setDrTxpow(DR_SF7,14);

  Serial.println("Started");

  do_send(&sendjob);
}

void loop() {
  attachInterrupts();
  os_runloop_once();
}

void initPins() {
  pinMode(PIN_BUTTON, INPUT);
  #ifdef LP_TPL
    pinMode(PIN_DONE, OUTPUT);
    pinMode(PIN_DRVN, INPUT_PULLUP);
    attachPinChangeInterrupt();
    Serial.println("Low power mode: TPL driven");
  #endif
  #ifdef LP_IRQ
    Serial.println("Low power mode: Interrupt driven (press D3 button to wake up)");
  #endif
  #ifdef LP_TIMER
    Serial.println("Low power mode: Timer");
  #endif
}

long readVcc() {
  long result;
  // Read 1.1V reference against AVcc
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Convert
  while (bit_is_set(ADCSRA, ADSC));
  result = ADCL;
  result |= ADCH << 8;
  result = 1126400L / result; // Back-calculate AVcc in mV
  return result;
}

void attachInterrupts() {
  #ifdef LP_IRQ
    attachInterrupt(digitalPinToInterrupt(PIN_BUTTON), wakeUp, FALLING);
  #endif
}

void sleep() {
  #if defined(LP_TPL) || defined(LP_TIMER) || defined(LP_IRQ)
    Serial.println("Enter sleep mode");
    delay(100);
    led.off();
    #ifdef LP_TPL
      // Tell the TPL5110 we are going to sleep (then waiting for a pulse on DRVN)
      digitalWrite(PIN_DONE, HIGH);
      delay(100);
      digitalWrite(PIN_DONE, LOW);
      delay(100);
      LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
      detachPinChangeInterrupt();
    #endif
    #ifdef LP_TIMER
      // Sleep for a fixed period of time
      LowPower.powerDown(LP_TIMER_SLEEP, ADC_OFF, BOD_OFF);
    #endif
    #ifdef LP_IRQ
      // Sleep forever and wait for an IRQ (button D3, INT1)
      LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
      detachInterrupt(digitalPinToInterrupt(PIN_BUTTON));
    #endif
  #endif
}

void wakeUp() {
  Serial.println("Wake up");
}

//================================================================================
// PCINT Definitions
//================================================================================

#define PCMSK *digitalPinToPCMSK(PIN_DRVN)
#define PCINT digitalPinToPCMSKbit(PIN_DRVN)
#define PCIE  digitalPinToPCICRbit(PIN_DRVN)
#define PCPIN *portInputRegister(digitalPinToPort(PIN_DRVN))

#if (PCIE == 0)
#define PCINT_vect PCINT0_vect
#elif (PCIE == 1)
#define PCINT_vect PCINT1_vect
#elif (PCIE == 2)
#define PCINT_vect PCINT2_vect
#else
#error This board doesnt support PCINT ?
#endif

volatile uint8_t oldPort = 0x00;

void attachPinChangeInterrupt(void) {
  // update the old state to the actual state
  oldPort = PCPIN;

  // pin change mask registers decide which pins are enabled as triggers
  PCMSK |= (1 << PCINT);

  // PCICR: Pin Change Interrupt Control Register - enables interrupt vectors
  PCICR |= (1 << PCIE);
}

void detachPinChangeInterrupt(void) {
  // disable the mask.
  PCMSK &= ~(1 << PCINT);

  // if that's the last one, disable the interrupt.
  if (PCMSK == 0)
    PCICR &= ~(0x01 << PCIE);
}

ISR(PCINT_vect) {
  // get the new and old pin states for port
  uint8_t newPort = PCPIN;

  // compare with the old value to detect a rising or falling
  uint8_t change = newPort ^ oldPort;

  // check which pins are triggered, compared with the settings
  uint8_t trigger = 0x00;
  #if (PCINT_MODE == RISING) || (PCINT_MODE == CHANGE)
    uint8_t rising = change & newPort;
    trigger |= (rising & (1 << PCINT));
  #endif
  #if (PCINT_MODE == FALLING) || (PCINT_MODE == CHANGE)
    uint8_t falling = change & oldPort;
    trigger |= (falling & (1 << PCINT));
  #endif

  // save the new state for next comparison
  oldPort = newPort;

  // if our needed pin has changed, call the IRL interrupt function
  if (trigger & (1 << PCINT))
    wakeUp();
}
